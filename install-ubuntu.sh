#!/usr/bin/env bash

source .env

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file})"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app

arg1="${1:-}"

readonly USER=${USER}
readonly GROUP=$(id -ng "$USER")
readonly HOME=${HOME}
readonly DISTRO=$(cat /etc/*-release | grep -i "^id=" | sed 's/[iI][dD]=//')
readonly RCOLOR='\033[0;31m'
readonly GCOLOR='\033[0;32m'
readonly NC='\033[0m'

REQUIREMENTS=0

after-install () {
    if [ -f "/usr/lib/systemd/user/dunst.service" ]; then
        sudo rm /usr/lib/systemd/user/dunst.service
    fi
    if [ -f "/usr/lib/systemd/user/mpd.service" ]; then
        sudo rm /usr/lib/systemd/user/mpd.service
    fi
    if [ -f "/usr/lib/systemd/user/mpd.socket" ]; then
        sudo rm /usr/lib/systemd/user/mpd.socket
    fi

    read -p "Do you want symlink for MPD music directory '/home/$ADMIN/Music -> /var/lib/mpd/music' (y/n)? " answer
    case ${answer:0:1} in
        y|Y )
            sudo rm -r /var/lib/mpd/music
            sudo ln -s /home/$ADMIN/Music /var/lib/mpd/music
            mpc update
        ;;
        * )
            echo "skipped creating symlink for MPD music directory"
        ;;
    esac

    read -p "Do you want disable GUI login (y/n)? " answer
    case ${answer:0:1} in
        y|Y )
            sudo apt-get -qq -y autoremove plymouth
            sudo systemctl set-default multi-user.target
            sudo sed -i 's/#GRUB_TERMINAL=console/GRUB_TERMINAL=console/g' /etc/default/grub
            sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=".*"/GRUB_CMDLINE_LINUX_DEFAULT="text"/g' /etc/default/grub
            sudo update-grub
        ;;
        * )
            echo "skipped disabling GUI login"
        ;;
    esac

    read -p "Do you want disable services (y/n)? " answer
    case ${answer:0:1} in
        y|Y )
            for serv in ${SERVICES[@]} ; do sudo systemctl disable $serv ; done
        ;;
        * )
            echo "skipped service deactivation"
        ;;
    esac

    read -p "Do you want prevent tearing on Intel GPU (y/n)? " answer
    case ${answer:0:1} in
        y|Y )
            sudo cp ${__dir}/files/20-intel.conf /etc/X11/xorg.conf.d/
            sudo chmod 644 /etc/X11/xorg.conf.d/20-intel.conf
            sudo chown root:root /etc/X11/xorg.conf.d/20-intel.conf
        ;;
        * )
            echo "skipped setting tearing"
        ;;
    esac
}

install-docker () {
	read -p "Do you want to install Docker (y/n)? " answer
	case ${answer:0:1} in
		y|Y )
			if [ $(grep -r 'download.docker.com' /etc/apt/sources.list.d | wc -l) -eq 0 ]; then
				install-requirements
				wget -qO - https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
				sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $UBUNTU_VER_DOCKER stable"
			fi
			sudo apt-get -qq update
			sudo apt-get -qq -y install docker-ce docker-compose
			set-group-docker
			sudo systemctl enable docker.service
		;;
		* )
			echo "Skipped installing Docker"
		;;
	esac

}

install-chrome () {
	read -p "Do you want to install Google Chrome (y/n)? " answer
	case ${answer:0:1} in
		y|Y )
			install-requirements
			wget -qO - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
			#sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
			sudo add-apt-repository "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"
			sudo apt-get -qq update
			sudo apt-get -qq -y install google-chrome-stable
		;;
		* )
			echo "Skipped installing Google Chrome"
		;;
	esac
}

install-libvirt () {
	read -p "Do you want to install Virt-Manager, Qemu, libvert and KVM (y/n)? " answer
	case ${answer:0:1} in
		y|Y )
			sudo apt-get -qq update
			sudo apt-get -qq -y install virt-manager
			set-group-kvm

		;;
		* )
			echo "Skipped installing Virt-Manager"
		;;
	esac
}

install-pkgs () {
	sudo apt-get -qq update
	sudo apt-get -qq upgrade
	for file in $PWD/pkgs/*; do xargs -a $file sudo apt-get -qq -y install ; done
}

install-requirements () {
	if [ "$REQUIREMENTS" -eq "0" ]; then
	   sudo apt-get -qq -y install apt-transport-https curl software-properties-common
	   REQUIREMENTS=1
	fi
}

install-virtualbox () {
	read -p "Do you want to install VirtualBox (y/n)? " answer
	case ${answer:0:1} in
		y|Y )
			install-requirements
			wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
			wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
			sudo add-apt-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $UBUNTU_VER contrib"
			sudo apt-get -qq update
			sudo apt-get -qq -y install virtualbox
		;;
		* )
			echo "Skipped installing VirtualBox"
		;;
	esac
}

set-group-docker () {
	for user in ${PROFILES[@]} ; do sudo usermod -a -G docker $user ; done
}

set-group-kvm () {
	for user in ${PROFILES[@]} ; do sudo usermod -a -G kvm $user ; done
}

configure-system () {
	if [ ! -d "/opt/i3-desktop" ]; then
		sudo mkdir /opt/i3-desktop
		sudo chown $USER:$USER /opt/i3-desktop
		git clone --quite https://gitlab.com/pavelhosek89/i3-desktop.git /opt/i3-desktop
	else
		cd /opt/i3-desktop; git pull --quiet; cd $__dir
	fi

    sudo cp ${__dir}/files/70-synaptics.conf /etc/X11/xorg.conf.d/
    sudo chmod 644 /etc/X11/xorg.conf.d/70-synaptics.conf
    sudo chown root:root /etc/X11/xorg.conf.d/70-synaptics.conf

    sudo cp ${__dir}/files/i3lock@.service /etc/systemd/system/
    sudo chmod 644 /etc/systemd/system/i3lock@.service
    sudo chown root:root /etc/systemd/system/i3lock@.service

	for direct in ${MNT_DIR[@]} ; do sudo mkdir -p /mnt/$direct ; done
	for user in ${PROFILES[@]} ; do create-user $user ; done
}

create-user () {
    if id -u $1 > /dev/null 2>&1; then
        echo "$1 exist"
    else
        sudo adduser --shell "/bin/bash" $1
        sudo chown -R $1:$1 /home/$1
    fi
	for dir in ${MKDIR[@]} ; do sudo mkdir -p /home/$1/$dir; sudo chown $1:$1 /home/$1/$dir ; done
	cd /opt/i3-desktop; sudo -H -u $1 bash -c './sync.sh -i'; cd $__dir
	for file in /home/$1/bin/*; do sudo chmod u+x $file ; done
	if [ -d "/home/$1/.vim/bundle/Vundle.vim" ]; then
		echo "Vundle.vim exist";
	else
		sudo git clone --quite https://github.com/VundleVim/Vundle.vim.git /home/$1/.vim/bundle/Vundle.vim;
		sudo chown -R $1:$1 /home/$1/.vim
        sudo -H -u $1 bash -c 'vim +PluginInstall +qall'
	fi
	sudo chown -Rf $1:$1 "/home/$1"
	sudo usermod -a -G audio $1
	sudo usermod -a -G video $1
    sudo systemctl enable i3lock@${USER}.service
}

### START SCRIPT ###
# the user must not be root
if [[ "$USER" = root ]]; then
	echo -e "${RCOLOR}$0: ERROR: USER is root!${NC}" >&2
	exit 1
fi
# only for Ubuntu
if [[ "$DISTRO" != ubuntu ]] ; then
	echo -e "${RCOLOR}$0: ERROR: Distribution is not Ubuntu!${NC}" >&2
	exit 1
fi
# if not installed Git
if [[ ! -f "/usr/bin/git" ]]; then
    echo -e "${RCOLOR}$0: ERROR: Git is not installed!${NC}" >&2
	exit 1
fi

configure-system

install-pkgs
install-docker
install-chrome
install-virtualbox
install-libvirt

after-install

read -p "Do you want reboot computer (y/n)? " answer
case ${answer:0:1} in
	y|Y )
		reboot
	;;
	* )
		echo "skipped restarting the computer"
	;;
esac
