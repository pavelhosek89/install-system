# install-system

Tested on Ubuntu 20.04, 20.10 and 21.04

## Install

* install Ubuntu minimal
  * [Ubuntu 20.04 "Focal Fossa" ](http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/mini.iso)
  * [How to install Ubuntu from Minimal CD (with UEFI)](https://www.onetransistor.eu/2015/12/install-ubuntu-minimal-cd-uefi-enabled.html)
* `sudo sed -i 's/Prompt=lts/Prompt=normal/g' /etc/update-manager/release-upgrades`
* `sudo do-release-upgrade`
* `sudo reboot`
* `sudo apt install git`
* `sudo mkdir /opt/install-system`
* `sudo chown $USER:$USER /opt/install-system`
* `git clone https://gitlab.com/pavelhosek89/install-system.git /opt/install-system`
* `cd /opt/install-system`
* `cp .env_sample .env` # and edit file
* `chmod a+x install-ubuntu.sh`
* add own packages for install
* `./install-ubuntu.sh`


## After install

Set GKT3 and GTK2 in `lxappearance` \
Install Sublime Text 3 [64 bit .deb](https://download.sublimetext.com/sublime-text_build-3211_amd64.deb)

* Sublime Text - install packages
  * BracketHighlighter
  * EditorConfig
  * Emmet
  * GitGutter
  * Increment Selection
  * Nette + Latte + Neon
  * PhpDoc
  * SFTP
  * SideBarEnhancements
  * Solarized Color Scheme
  * TrailingSpaces
  * Xdebug Client
